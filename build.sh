#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

readonly pfx="$PWD/local"
readonly tmp="$PWD/tmp"
mkdir -p "$pfx"
mkdir -p "$tmp"

# build fs2open
#
pushd "source"
mkdir -p build
cd build
export PKG_CONFIG_PATH="$pfx/lib/pkgconfig"
cmake \
    -DCMAKE_PREFIX_PATH="$pfx" \
    -DCMAKE_BUILD_TYPE=MinSizeRel \
    ..
make -j "$(nproc)"
DESTDIR="$tmp" make install
popd

mkdir -p "273620/dist/lib/"
cp -rfv "local/"lib/*.so* "273620/dist/lib/"
cp -rfv "source/build/bin/fs2_open_20_1_0_x64" "273620/dist/fs2_open_x64"
cp -rfv "run-freespace2.sh" "273620/dist/run-freespace2.sh"
